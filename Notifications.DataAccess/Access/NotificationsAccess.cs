﻿using Notifications.Common.Interfaces;
using Notifications.Common.Models;
using Notifications.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Notifications.DataAccess.Access
{
    public class NotificationsAccess : INotificationsAccess
    {
        private readonly NotificationsDbContext dbContext;

        public NotificationsAccess(NotificationsDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void AddNotification(NotificationModel notification)
        {
            NotificationEntity notificationEntity = new NotificationEntity()
            {
                Id = notification.Id,
                EventType = (int)notification.EventType,
                Message = notification.Message,
                Title = notification.Title,
                userId = notification.UserId
            };


            dbContext.Notifications.Add(notificationEntity);
            dbContext.SaveChanges();
        }

        public IEnumerable<NotificationModel> GetAllNotifications(int userId)
        {
            return dbContext.Notifications
                .Where(n => n.userId == userId)
                .Select(entity => NotificationModel
                .Create(entity.Id, (EventTypeEnum)entity.EventType, entity.Message, entity.Title, entity.userId));
                
        }
    }
}
