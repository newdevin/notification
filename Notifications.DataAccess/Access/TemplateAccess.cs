﻿using Notifications.Common.Interfaces;
using Notifications.Common.Models;
using Notifications.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Notifications.DataAccess.Access
{
    public class TemplateAccess : ITemplateAccess
    {
        readonly NotificationsDbContext dbContext;

        public TemplateAccess(NotificationsDbContext notificationsDbContext)
        {
            dbContext = notificationsDbContext;
        }
        public TemplateModel GetTemplate(EventTypeEnum eventType)
        {
            TemplateEntity templateEntity = dbContext.Templates.FirstOrDefault(t => t.EventType == (int) eventType);
            //  null handling if template not found

            return TemplateModel.Create(templateEntity?.Id ?? Guid.NewGuid(), templateEntity?.Body, templateEntity?.Title);
            

        }
    }
}
