﻿using Notifications.Common.Interfaces;
using Notifications.Common.Models;
using Notifications.DataAccess.Entities;
using System.Linq;

namespace Notifications.DataAccess.Access
{
    public class UserAccess : IUserAccess
    {
        readonly NotificationsDbContext dbContext;

        public UserAccess(NotificationsDbContext notificationsDbContext)
        {
            dbContext = notificationsDbContext;
        }

        public UserModel GetUser(int userId)
        {
            UserEntity userEntity = dbContext.Users.FirstOrDefault(u => u.Id == userId);
            // :TODO null handling if user not found
            return UserModel.Create(userEntity.Id, userEntity.FirstName, userEntity.LastName);
            
        }
    }
}
