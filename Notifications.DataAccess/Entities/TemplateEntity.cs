﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Notifications.DataAccess.Entities
{
    public class TemplateEntity
    {
        public Guid Id { get; set; }
        public int EventType { get; set; }
        public string Body { get; set; }
        public string Title { get; set; }

    }
}
