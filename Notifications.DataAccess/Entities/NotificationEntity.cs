﻿using System;
using Notifications.Common.Models;

namespace Notifications.DataAccess.Entities
{
    public class NotificationEntity
    {
        public Guid Id { get; set; }
        public int EventType { get; set; }
        public string Message { get; set; }
        public string Title { get; set; }
        public int userId { get; set; }
    }
}
