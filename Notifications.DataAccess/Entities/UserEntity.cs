﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Notifications.DataAccess.Entities
{
    public class UserEntity
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }
}
