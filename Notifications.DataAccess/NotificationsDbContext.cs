﻿using Microsoft.EntityFrameworkCore;
using Notifications.DataAccess.Entities;
using System;

namespace Notifications.DataAccess
{
    public class NotificationsDbContext : DbContext
    {
        public NotificationsDbContext(DbContextOptions<NotificationsDbContext> options)
            : base(options)
        { }

        public DbSet<NotificationEntity> Notifications { get; set; }
        public DbSet<TemplateEntity> Templates { get; set; }
        public DbSet<UserEntity> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<UserEntity>().HasData(
                new UserEntity()
                {
                    Id = 1,
                    FirstName = "Devinder",
                    LastName = "Singh"
                });
            modelBuilder.Entity<TemplateEntity>().HasData(

                new TemplateEntity
                {
                    Id = Guid.NewGuid(),
                    EventType = 1,
                    Body= "Hi {Firstname}, your appointment with {OrganisationName} at {AppointmentDateTime} has been - cancelled for the following reason: {Reason}.",
                    Title = "Appointment Cancelled"
                    
                }

                );

        }
    }
}
