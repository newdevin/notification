﻿using Moq;
using Notifications.Common.Interfaces;
using Notifications.Common.Models;
using Notifications.DataAccess.Access;
using Notifications.Services;
using System;
using Xunit;
using static System.Guid;

namespace Notifiactions.Services.Tests
{
    public class NotificationsServiceTests
    {
        Mock<INotificationsAccess> notificationAccessMock;
        Mock<ITemplateAccess> templateAccessMock;
        Mock<IUserAccess> userAccessMock;

        public NotificationsServiceTests()
        {
            notificationAccessMock = new Mock<INotificationsAccess>();
            templateAccessMock = new Mock<ITemplateAccess>();
            userAccessMock = new Mock<IUserAccess>();
        }

        [Fact]
        public void SaveNotificationCallsTheNotificationAccessAddNotificationsMethod()
        {
            var eventModel = GetEventModel();
            var templateModel = GetTemplateModel();
            int userId = 1;

            templateAccessMock
                .Setup(m => m.GetTemplate(It.IsAny<EventTypeEnum>()))
                .Returns(templateModel);

            var notificationService = new NotificationsService(notificationAccessMock.Object, templateAccessMock.Object, userAccessMock.Object);

            notificationService.SaveNotification(EventTypeEnum.AppointmentCancelled, eventModel, userId);

            notificationAccessMock.Verify(m => m.AddNotification(It.IsAny<NotificationModel>()));


        }

        private static EventModel GetEventModel()
        {
            string firstName = "Derek";
            DateTime appointmentDateTime = DateTime.Now.AddDays(1);
            string organisationName = "Northwind";
            string reason = "holiday";
            var eventModel = EventModel.Create(firstName, appointmentDateTime, organisationName, reason);
            return eventModel;
        }

        private TemplateModel GetTemplateModel()
        {
            var id = NewGuid();
            string title = "Template title";
            string body = "Body of the template";

            return TemplateModel.Create(id, title, body);

        }
    }
}
