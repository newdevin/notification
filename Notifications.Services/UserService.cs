﻿using Notifications.Common.Interfaces;
using Notifications.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Notifications.Services
{
    public class UserService : IUserService
    {
        readonly IUserAccess userAccess;

        public UserService(IUserAccess userAccess)
        {
            this.userAccess = userAccess;
        }

        public UserModel GetUser(int userId)
        {
            return userAccess.GetUser(userId);
        }
    }
}
