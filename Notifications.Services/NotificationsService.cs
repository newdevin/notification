﻿using Notifications.Common.Interfaces;
using Notifications.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Notifications.Services
{
    public class NotificationsService : INotificationsService
    {
        private readonly INotificationsAccess notificationsAccess;
        readonly IUserAccess userAccess;
        readonly ITemplateAccess templateAccess;

        public NotificationsService(INotificationsAccess notificationsAccess, ITemplateAccess templateAccess, IUserAccess userAccess)
        {
            this.templateAccess = templateAccess;
            this.userAccess = userAccess;
            this.notificationsAccess = notificationsAccess;
        }

        public IReadOnlyCollection<NotificationModel> GetAllNotifications(int userId)
        {
            return notificationsAccess.GetAllNotifications(userId).ToList();
        }

        public void SaveNotification(EventTypeEnum eventType, EventModel eventModel, int userId)
        {
            TemplateModel template = templateAccess.GetTemplate(eventType);
            //suitable exception handling for null;
            if (template != null)
            {
                NotificationModel notification = NotificationModel
                    .Create(Guid.NewGuid(), eventType, template.PopulateBody(eventModel), template.Title, userId);

                notificationsAccess.AddNotification(notification);

            }
        }
    }
}
