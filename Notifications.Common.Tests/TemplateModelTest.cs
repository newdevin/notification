﻿using Notifications.Common.Models;
using System;
using Xunit;
using static System.Guid;

namespace Notifications.Common.Tests
{
    public class TemplateModelTest
    {
        [Fact]
        public void ExpectArgumentExceptionWhenTitleIsNull()
        {
            var id = NewGuid();
            string title = null;
            var body = "Body of templat";

            Assert.Throws<ArgumentException>(() => TemplateModel.Create(id, title, body ));

        }

        [Fact]
        public void ExpectArgumentExceptionWhenBodyIsNull()
        {
            var id = NewGuid();
            string title = "Template title";
            string body = null;

            Assert.Throws<ArgumentException>(() => TemplateModel.Create(id, title, body));

        }

        [Fact]
        public void TemplateModelIsCreatedSuccessfully()
        {
            var id = NewGuid();
            string title = "Template title";
            string body = "Body of the template";

            var templateModel = TemplateModel.Create(id, title, body);
            Assert.NotNull(templateModel);

        }

        [Fact]
        public void TemplateBodyIsPupulatedWithCorrectData()
        {
            string firstName = "Derek";
            DateTime appointmentDateTime = new DateTime(2019,04,01);
            string organisationName = "NHS";
            string reason = "holiday";

            var eventModel = EventModel.Create(firstName, appointmentDateTime, organisationName, reason);

            var id = NewGuid();
            string title = "Appointment Cancellation";
            string body = "Hi {Firstname}, your appointment with {OrganisationName} at {AppointmentDateTime} has been - cancelled for the following reason: {Reason}.";
                       
            var expected = "Hi Derek, your appointment with NHS at 01/04/2019 has been - cancelled for the following reason: holiday.";

            var templateModel = TemplateModel.Create(id, title, body);
            var actual = templateModel.PopulateBody(eventModel);

            Assert.Equal(expected, actual);
            

        }
    }
}
