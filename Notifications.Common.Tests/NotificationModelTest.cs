﻿using Notifications.Common.Models;
using System;
using Xunit;

namespace Notifications.Common.Tests
{
    public class NotificationModelTest
    {
        [Fact]
        public void ThrowsArgumentExceptionWhenEventTypeEnumIsInvalid()
        {
            var id = Guid.NewGuid();
            var eventType = (EventTypeEnum)10;
            var message = "Message for you";
            var title = "Cancellation of appointment";
            var userId = 2;

            Assert.Throws<ArgumentException>(() => NotificationModel.Create(id, eventType, message, title, userId));

        }

        [Fact]
        public void ThrowsArgumentExceptionWhenMessageIsNull()
        {
            var id = Guid.NewGuid();
            var eventType = EventTypeEnum.AppointmentCancelled;
            string message = null;
            var title = "Cancellation of appointment";
            var userId = 2;

            Assert.Throws<ArgumentException>(() => NotificationModel.Create(id, eventType, message, title, userId));

        }

        [Fact]
        public void ThrowsArgumentExceptionWhenTitleIsNull()
        {
            var id = Guid.NewGuid();
            var eventType = EventTypeEnum.AppointmentCancelled;
            string message = "Appointment cancellation";
            string title = null;
            var userId = 2;

            Assert.Throws<ArgumentException>(() => NotificationModel.Create(id, eventType, message, title, userId));

        }


        [Fact]
        public void NotificationModelIsCreatedSuccessfully()
        {
            var id = Guid.NewGuid();
            var eventType = EventTypeEnum.AppointmentCancelled;
            string message = "Important message";
            var title = "Cancellation of appointment";
            var userId = 2;

            var notificationModel = NotificationModel.Create(id, eventType, message, title, userId);
            Assert.NotNull(notificationModel);

        }
    }
}
