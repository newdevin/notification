﻿using Notifications.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Notifications.Common.Tests
{
    public class EventModelTest
    {
        [Fact]
        public void ExpectArgumentExceptionWhenFirstNameIsNull()
        {
            string firstName = null;
            DateTime appointmentDateTime = DateTime.Now.AddDays(1);
            string organisationName = "Northwind";
            string reason = "holiday";

            Assert.Throws<ArgumentException>(() => EventModel.Create(firstName, appointmentDateTime, organisationName, reason));
        }

        [Fact]
        public void ExpectArgumentExceptionWhenOrganinationNameIsNull()
        {
            string firstName = "Derek";
            DateTime appointmentDateTime = DateTime.Now.AddDays(1);
            string organisationName = null;
            string reason = "holiday";

            Assert.Throws<ArgumentException>(() => EventModel.Create(firstName, appointmentDateTime, organisationName, reason));
        }

        [Fact]
        public void ExpectArgumentExceptionWhenReasonIsNull()
        {
            string firstName = "Derek";
            DateTime appointmentDateTime = DateTime.Now.AddDays(1);
            string organisationName = "Northwind";
            string reason = null;

            Assert.Throws<ArgumentException>(() => EventModel.Create(firstName, appointmentDateTime, organisationName, reason));
        }

        [Fact]
        public void EventModelIsCreatedSuccessfully()
        {
            string firstName = "Derek";
            DateTime appointmentDateTime = DateTime.Now.AddDays(1);
            string organisationName = "Northwind";
            string reason = "holiday";

            var eventModel = EventModel.Create(firstName, appointmentDateTime, organisationName, reason);
            Assert.NotNull(eventModel);
        }
    }
    
}
