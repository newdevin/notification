﻿using Notifications.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Notifications.Common.Tests
{
    public class UserModelTest
    {
        [Fact]
        public void ExpectArgumentExceptionWhenFirstNameIsNull()
        {
            string firstName = null;
            var lastName = "Smith";
            int id = 10;

            Assert.Throws<ArgumentException>(() => UserModel.Create(id, firstName, lastName));
        }

        [Fact]
        public void ExpectArgumentExceptionWhenLastNameIsNull()
        {
            string firstName = "Derek";
            string lastName = null;
            int id = 10;

            Assert.Throws<ArgumentException>(() => UserModel.Create(id, firstName, lastName));
        }

        [Fact]
        public void UserModelIsCreatedSuccessfully()
        {
            string firstName = "Derek";
            var lastName = "Smith";
            int id = 10;

            var user = UserModel.Create(id, firstName, lastName);
            Assert.NotNull(user);
        }
    }
}
