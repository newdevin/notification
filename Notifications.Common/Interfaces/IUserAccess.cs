﻿using Notifications.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Notifications.Common.Interfaces
{
    public interface IUserAccess
    {
        UserModel GetUser(int userId);
    }
}
