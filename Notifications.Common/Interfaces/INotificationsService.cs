﻿using Notifications.Common.Models;
using System.Collections.Generic;

namespace Notifications.Common.Interfaces
{
    public interface INotificationsService
    {

        void SaveNotification(EventTypeEnum eventType, EventModel eventModel, int userId);
        IReadOnlyCollection<NotificationModel> GetAllNotifications(int userId);
    }
}
