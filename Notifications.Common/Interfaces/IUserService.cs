﻿using Notifications.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Notifications.Common.Interfaces
{
    public interface IUserService
    {
        UserModel GetUser(int userId);
    }
}
