﻿using Notifications.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Notifications.Common.Interfaces
{
    public interface ITemplateAccess
    {
        TemplateModel GetTemplate(EventTypeEnum templateType);
    }
}
