﻿using System;

namespace Notifications.Common.Models
{
    public class UserModel
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public int Id { get; private set; }

        private UserModel()
        {

        }

        public static UserModel Create(int userId, string firstName, string lastName)
        {
            if (string.IsNullOrEmpty(firstName))
                throw new ArgumentException($"Please provide a valid {nameof(firstName)}");
            if (string.IsNullOrEmpty(lastName))
                throw new ArgumentException($"Please provide a valid {nameof(lastName)}");

            return new UserModel { Id = userId, FirstName = firstName, LastName = lastName };
        }
    }
}
