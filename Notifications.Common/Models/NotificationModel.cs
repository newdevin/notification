﻿using System;

namespace Notifications.Common.Models
{
    public class NotificationModel
    {
        public Guid Id { get; private set; }
        public EventTypeEnum EventType { get; private set; }
        public string Message { get; private set; }
        public string Title { get; private set; }
        public int UserId { get; private set; }

        private NotificationModel()
        {

        }

        public static NotificationModel Create(Guid id, EventTypeEnum eventType, string message, string title, int userId)
        {
            if (!Enum.IsDefined(typeof(EventTypeEnum), eventType))
                throw new ArgumentException($"Please provide a valid {nameof(eventType)}");
            if (string.IsNullOrEmpty(message))
                throw new ArgumentException($"Please provide a valid {nameof(message)}");
            if (string.IsNullOrEmpty(title))
                throw new ArgumentException($"Please provide a valid {nameof(title)}");

            return new NotificationModel { Id = id, EventType = eventType, Message = message, Title = title, UserId = userId };

        }

    }

}
