﻿using System;

namespace Notifications.Common.Models
{
    public class EventModel
    {
        public string FirstName { get; private set; }
        public DateTime AppointmentDateTime { get; private set; }
        public string OrgsanisationName { get; private set; }
        public string Reason { get; private set; }


        private EventModel()
        {

        }

        public static EventModel Create(string firstName, DateTime appointmentDateTime, string organisationName, string reason)
        {
            if (string.IsNullOrEmpty(firstName))
                throw new ArgumentException($"Please provide a valid {nameof(firstName)}");
            if (string.IsNullOrEmpty(organisationName))
                throw new ArgumentException($"Please provide a valid {nameof(organisationName)}");
            if (string.IsNullOrEmpty(reason))
                throw new ArgumentException($"Please provide a valid {nameof(reason)}");

            return new EventModel
            {
                FirstName = firstName,
                AppointmentDateTime = appointmentDateTime,
                OrgsanisationName = organisationName,
                Reason = reason
            };
        }
    }

    public enum EventTypeEnum
    {
        AppointmentCancelled = 0
    }
}
