﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Notifications.Common.Models
{
    public class TemplateModel
    {
        public Guid Id { get; private set; }
        public string Title { get; private set; }
        public string Body { get; private set; }

        private TemplateModel()
        {

        }
        public static TemplateModel Create(Guid id, string title, string body)
        {
            if (string.IsNullOrEmpty(title))
                throw new ArgumentException($"Please provide a valid {title}");
            if (string.IsNullOrEmpty(body))
                throw new ArgumentException($"Please provide a valid {body}");

            return new TemplateModel { Id = id, Title = title, Body = body };
        }

        public string PopulateBody(EventModel eventModel)
        {
            return Body
                ?.Replace("{Firstname}", eventModel.FirstName)
                ?.Replace("{OrganisationName}", eventModel.OrgsanisationName)
                ?.Replace("{AppointmentDateTime}", eventModel.AppointmentDateTime.ToShortDateString())
                ?.Replace("{Reason}", eventModel.Reason);
        }
    }
}
