﻿using Microsoft.AspNetCore.Mvc;
using Notifications.Common.Interfaces;
using Notifications.Common.Models;
using System;
using System.Collections.Generic;

namespace Notifications.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationsController : ControllerBase
    {
        private readonly INotificationsService _notificationsService;

        public NotificationsController(INotificationsService notificationsService)
        {
            this._notificationsService = notificationsService;
        }

        [HttpGet, Route("get/{userId}")]
        public IReadOnlyCollection<NotificationModel> Get(int userId)
        {
            return _notificationsService.GetAllNotifications(userId);
        }


        [Route("add-event")]
        [HttpPost]
        public IActionResult AddEvent(int eventType, EventModel eventModel, int userId)
        {
            if (!IsValidEventType(eventType) || !IsValidEventModel(eventModel))
            {
                return BadRequest("event type is not valid");
            }


            _notificationsService.SaveNotification((EventTypeEnum)eventType, eventModel, userId);

            return Ok();
        }

        private bool IsValidEventModel(EventModel eventModel)
        {
            if (null == eventModel)
                return false;

            if (string.IsNullOrEmpty(eventModel.FirstName))
                return false;

            if (string.IsNullOrEmpty(eventModel.OrgsanisationName))
                return false;

            if (string.IsNullOrEmpty(eventModel.Reason))
                return false;

            return true;
        }

        private bool IsValidEventType(int eventType)
        {
            if (Enum.IsDefined(typeof(EventTypeEnum), eventType))
                return true;
            return false;

        }

    }
}