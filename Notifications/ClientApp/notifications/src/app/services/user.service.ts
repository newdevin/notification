import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { IUser } from '../interfaces/user';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {

   }

  getUser(): Observable<IUser> {
    return this.http.get<IUser>('/api/user/1');
  }

}
