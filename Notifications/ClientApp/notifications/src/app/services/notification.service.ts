import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { INotification } from '../interfaces/notification';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private http: HttpClient) {

  }

  getNotification():Observable<INotification[]>{
    return this.http.get<INotification[]> ('/api/notifications/get/1');
  }
  
}
