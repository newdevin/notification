import { Component, OnInit } from '@angular/core';
import { UserService } from './services/user.service';
import { IUser } from './interfaces/user';
import { NotificationService } from './services/notification.service';
import { INotification } from './interfaces/notification';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    
  user:IUser;
  notifications:INotification[];

  async ngOnInit() {
    this.user = await this.userService.getUser().toPromise();
    this.notifications = await this.notificationService.getNotification().toPromise();
  }
  
  constructor(private userService: UserService, private notificationService:NotificationService) {
    

  }
  title = 'notifications';
}
